import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import * as THREE from 'three'
import { renderer as rendererTS } from '../src/ts/renderer'
import { camera as cameraTS } from '../src/ts/camera'
import { scene as sceneTS } from '../src/ts/scene'
import { loadFbx, loadGltf, loadObjMtl, loadTexture } from '../src/ts/loaders'
// import { ambientLight, light } from '../src/ts/light'
import { AmbientLight, DirectionalLight } from 'three'

export const helperGroup = new THREE.Group()
let camera = cameraTS, controls: any, scene = sceneTS.clone(), renderer = rendererTS
// 辅助线
const axesHelper = new THREE.AxesHelper(100);
scene.add(axesHelper);
scene.add(helperGroup);
animate();
init()

async function init() {
    camera.position.set(500, 500, 500)
    camera.updateMatrixWorld()
    document.body.appendChild(renderer.domElement);
    controls = new OrbitControls(camera, renderer.domElement)
    controls.addEventListener('change',()=>{
        console.log(camera.position)
    })
    const ambientLight = new AmbientLight(0x707070);

    let light = new DirectionalLight(0xffffff, 4);
    light.position.set(  1181.4843537287065,  1580.039682987524, 1809.053759094986);
    light.castShadow = true;
    const d = 140;
    light.shadow.camera.left = - d;
    light.shadow.camera.right = d;
    light.shadow.camera.top = d;
    light.shadow.camera.bottom = - d;

    light.shadow.camera.near = 2;
    light.shadow.camera.far = 10;

    light.shadow.mapSize.x = 102;
    light.shadow.mapSize.y = 102;

    scene.add(light)
    scene.add(ambientLight)
   
}

// 循环渲染
function animate() {
    requestAnimationFrame(animate);
    render();
}

// 渲染函数
function render() {
    renderer.render(scene, camera);
}


// 立方体
const cubeGeometry = new THREE.BoxGeometry(10, 10, 10);
const cubeMaterial = new THREE.MeshLambertMaterial({ color: 0x00ff00 });
const cube = new THREE.Mesh(cubeGeometry, cubeMaterial);
scene.add(cube)